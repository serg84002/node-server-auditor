(function (app) {
	app.controller('reportCtrl', ['$scope', '$routeParams', '$query', function ($scope, $routeParams, $query) {
			$scope.state = {
				isLoading: false,
				loaded: false
			};

			$scope.errorText = false;

			$scope.loadReport = function (recent) {
				recent = recent || false;

				$scope.state.isLoading = true;
				$scope.errorText = false;

				$query('reports/' + $routeParams.name, {recent: recent}).then(function (res) {
					var response = res.response;

					$scope.report = response;
					$scope.state.loaded = true;
					
				}, function (res) {
					var response;
					
					if(!res.data){
						$scope.state.loaded = false;
						return;
					}
					
					response = res.data.data;

					$scope.report = response;


					switch (true) {
						case response.result && !!response.result.message:
							$scope.errorText = response.result.message;
							break;
						case response.result && !!response.result.code:
							$scope.errorText = response.result.code;
							break;
						case !!response.error:
							$scope.errorText = response.error;
							break;
						default:
							$scope.errorText = res.data.response;
					}
					
					$scope.state.loaded = true;
					console.error(res.data);
				}).finally(function () {
					$scope.state.isLoading = false;
				});
			};


			$scope.loadReport();


		}]);
})(app);