(function (module) {
	var Sequelize        = require('sequelize');
	var default_database = require('src/node/database')();

	var TReport = default_database.define('t_report',
	{
		// column 'id_report'
		id_report: {
			field:         "id_report",
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       "id_report - primary key",
			validate:      {
			}
		},

		//column 'report_name'
		report_name: {
			field:     "report_name",
			type:      Sequelize.STRING,
			allowNull: false,
			comment:   "report_name - name of report",
			validate:  {
				notEmpty: true // don't allow empty strings
			}
		},

		is_active: {
			field:     "is_active",
			type:      Sequelize.BOOLEAN,
			allowNull: false,
			comment:   "for soft delete of row",
			defaultValue: true
		}
	},
	{
		// define the table's name
		tableName: 't_report',

		comment: "list of reports",


		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			TReport.belongsTo(
				models.t_module, {
					foreignKey: 'id_module'
				}
			);
		},

		indexes: [
		]
	}
	);

	module.exports = TReport;
})(module);
