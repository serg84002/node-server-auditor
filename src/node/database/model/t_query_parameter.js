(function (module) {
	var Sequelize = require('sequelize');
	var database  = require('src/node/database')();

	var db_query_parameters = database.define('t_query_parameter',
		{
			// column 'id_query_parameter'
			id_query_parameter: {
				field:         "id_query_parameter",
				type:          Sequelize.INTEGER,
				allowNull:     false,
				primaryKey:    true,
				autoIncrement: true,
				comment:       "query_parameters id - primary key",
				validate:      {
				}
			},
			// column 'query_parameter_name'
			query_parameter_name:{
				field:     "query_parameter_name",
				type:      Sequelize.STRING,
				allowNull: false,
				comment:   "external server connection type",
				validate: {
					notEmpty: true // don't allow empty strings
				}
			},
			
			is_active: {
				field:     "is_active",
				type:      Sequelize.BOOLEAN,
				allowNull: false,
				comment:   "for soft delete of row",
				defaultValue: true
			},


			// // column 'connType'
			// connType:{
			// 	field:     "connType",
			// 	type:      Sequelize.STRING,
			// 	allowNull: false,
			// 	comment:   "external server connection type",
			// 	validate: {
			// 		notEmpty: true // don't allow empty strings
			// 	}
			// },

			// // column 'connName'
			// connName:{
			// 	field:     "connName",
			// 	type:      Sequelize.STRING,
			// 	allowNull: false,
			// 	comment:   "external server connection name",
			// 	validate: {
			// 		notEmpty: true // don't allow empty strings
			// 	}
			// },

			// // column 'repName'
			// repName:{
			// 	field:     "repName",
			// 	type:      Sequelize.STRING,
			// 	allowNull: false,
			// 	comment:   "report name",
			// 	validate:  {
			// 		notEmpty: true // don't allow empty strings
			// 	}
			// },

			// // column 'nameParam'
			// nameParam:{
			// 	field:     "nameParam",
			// 	type:      Sequelize.STRING,
			// 	defaultValue: "",
			// 	comment:   "Name parameter",
			// },

			// // column 'valueParam'
			// valueParam:{
			// 	field:        "valueParam",
			// 	type:         Sequelize.STRING,
			// 	allowNull:    true,
			// 	defaultValue: "",
			// 	comment:      "Value parameter",
			// 	validate:     {
			// 					}
			// }
		},
		{
			// define the table's name
			tableName: 't_query_parameter',

			comment: 'table changed parameter for reports',

			// 'createdAt' to actually be called '_datetime_created'
			createdAt: '_datetime_created',

			// 'updatedAt' to actually be called '_datetime_updated'
			updatedAt: '_datetime_updated',

			// disable the modification of table names; By default, sequelize will automatically
			// transform all passed model names (first parameter of define) into plural.
			freezeTableName: true,

			charset: 'utf8',


			associate: function( models ) {
				db_query_parameters.belongsTo(
					models.t_query, {
						foreignKey: 'id_query'
					}
				);
			},

			indexes: [
				// {
					// name:   'uidx_db_query_parameters',
					// unique: true,
					// fields: [
					// 		'connType',
					// 		'connName',
					// 		'repName',
					// 		'nameParam'
					//	]
				// }
			]
		}
	);

	db_query_parameters.sync({ force: false });

	module.exports = db_query_parameters;
})(module);
