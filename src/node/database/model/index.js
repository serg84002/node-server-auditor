(function (module) {
	var tables = {
		t_connection:           			require('./t_connection'),
		dbSettScheduler:      			require('./dbSettScheduler'),
		db_query_parameter: 			require('./t_query_parameter'),
		t_connection_type:    			require('./t_connection_type'),
		t_module:             			require('./t_module'),
		t_query:              			require('./t_query'),
		t_query_result_table: 			require('./t_query_result_table'),
		t_report: 						require('./t_report'),
		t_connection_query_parameter: 	require('./t_connection_query_parameter')
	};

	for(var foreignKeyTable in tables) {
		if (tables[foreignKeyTable].options.hasOwnProperty("associate")) {
			tables[foreignKeyTable].options.associate(tables);
		}
	}

	module.exports = tables;
})(module);
