(function (module) {
	var Sequelize        = require('sequelize');
	var default_database = require('src/node/database')();

	var TCQPtable = default_database.define('t_connection_query_parameter',
	{
		id_connecton_query_parameter: {
			field:         "id_connecton_query_parameter",
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       "t_connection_query_parameter id - primary key",
			validate:      {
			}
		},

		// column 'id'
		query_parameter_value: {
			field:         "query_parameter_value",
			type:          Sequelize.INTEGER,
			allowNull:     true,
			comment:       "query parameter value",
			validate:      {
			}
		},
	},
	{
		// define the table's name
		tableName: 't_connection_query_parameter',

		comment: "",

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			TCQPtable.belongsTo(
				models.t_connection, { // TODO
					foreignKey: 'id_connection' // TODO
				}
			);
			TCQPtable.belongsTo(
				models.db_query_parameter, {
					foreignKey: 'id_query_parameter'
				}
			);
		},

		indexes: [
		]
	}
	);

	module.exports = TCQPtable;
})(module);
