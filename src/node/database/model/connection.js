(function (module) {
	var Sequelize        = require('sequelize');
	var default_database = require('src/node/database')();

	var TConnection = module.exports = default_database.define('connection',
	{
		// column 'id'
		id: {
			field:         "id",
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       "connection id - primary key",
			validate:      {
			}
		},

		// column 'serverName'
		serverName: {
			field:     "serverName",
			type:      Sequelize.STRING,
			allowNull: false,
			comment:   "external server name as the data source (unique)",
			validate:  {
				notEmpty: true // don't allow empty strings
			}
		}
	},
	{
		// define the table's name
		tableName: 'connections',

		comment: "list of connections to external data sources",

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
		},

		indexes: [
			{
				name:   'uidx_connections_serverName',
				unique: true,
				fields: [
					'serverName'
				]
			}
		]
	}
	);

	module.exports = TConnection;
})(module);
