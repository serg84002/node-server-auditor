(function (module) {
	var util = require('util');
	var Model = require('./');

	var DB_Configuration = function (params) {
		DB_Configuration.super_.apply(this, arguments);

		this.set('server', params.server);
		this.set('module', params.module);

	};

	util.inherits(DB_Configuration, Model);

	module.exports = DB_Configuration;
})(module);