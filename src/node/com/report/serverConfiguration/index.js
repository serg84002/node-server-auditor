(function(module){
	var ServerConfiguration = {
		getAvailable:     require('./availableServers'),
		getConfiguration: require('./getConfig')
	};

	module.exports = ServerConfiguration;
})(module);
