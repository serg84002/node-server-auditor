(function (module) {
	var Q = require('q'),
			destroy = function (model, params) {
				var dfd = Q.defer();
				params = params || {};

				if (params.isAll) {
					model.drop()
							.finally(dfd.resolve);
				} else {
					model.destroy({where: {connectionId: params.connection.get('id')}})
							.finally(dfd.resolve);
				}

				return dfd.promise;
			}
	;
	module.exports = destroy;
})(module);