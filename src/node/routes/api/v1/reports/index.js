(function (module) {
	var handler = null;
	var express = require('express');
	var router  = express.Router();
	var Data    = require('src/node/com/data');
	var Report  = require('src/node/com/report');

	router.get('/', function (req, res) {
		var list = Report(req.sessionId).list();
		
		if (list) {
			res.json(handler.response(list));
		} else {
			res.json(handler.error('not configured'));
		}

	});

	router.get('/json', function (req, res) {
		var list = Report(req.sessionId).jsonMenu();

		if (list) {
			res.json(handler.response(list));
		} else {
			res.json(handler.error('not configured'));
		}
	});

	router.get('/:name', function (req, res) {
		var recent = req.query.recent === 'true';
		console.log(req.params);
		Report(req.sessionId).getReport(req.params.name, recent).then(

			function (result) {
				res.json(handler.response(result));
			},
			function (err) {
				res.json(handler.error('report error', err));
			}
		);

	});

	module.exports = function (resHandler) {
		handler = resHandler;
		return router;
	};
})(module);
