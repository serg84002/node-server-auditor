(function (module) {
	var handler;
	var express = require('express');
	var router = express.Router();

	var Data = require('src/node/com/data');
	var Report = require('src/node/com/report');
	var ReportModuleFactory = require('src/node/com/report/reportModuleFactory');
	var ServerConfiguration = require('src/node/com/report/serverConfiguration');

	router.get('/', function (req, res) {

	});

	router.get('/reports', function (req, res) {

		ReportModuleFactory.getAvailableExtended().then( function( data ) {
			res.json(handler.response({
				config: Report(req.sessionId).isConfigured() ? Report(req.sessionId).config.data() : null,
				modules: data,
				servers: ServerConfiguration.getAvailable()
			}));
		} );
	});

	router.put('/reports', function (req, res) {
		var config;

		if (!Object.keys(req.body).length) {
			res.json(handler.response(Report(req.sessionId).configure(null)));
			return;
		}

		config = new Data.DB_Configuration({
			server: ServerConfiguration.getConfiguration(req.body.server),
			module: req.body.module
		});

		if (config.isValid()) {
			res.json(handler.response(Report(req.sessionId).configure(config)));
		} else {
			res.json(handler.error('wrong configuration'));
		}
	});

	module.exports = function (resHandler) {
		handler = resHandler;
		return router;
	};
})(module);