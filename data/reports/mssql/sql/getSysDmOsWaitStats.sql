SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

SELECT
	 getdate()        AS [EventTime]
	,t.[wait_type]    AS [WaitType]
	,t.[wait_time_ms] AS [WaitTimeMs]
FROM
	[sys].[dm_os_wait_stats] t
WHERE
	t.[wait_type] IS NOT NULL
	AND t.[wait_time_ms] IS NOT NULL
	AND t.[wait_time_ms] > 0
;
